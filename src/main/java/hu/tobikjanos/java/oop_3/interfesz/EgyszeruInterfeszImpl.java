/*
============================================================================
Name        : EgyszeruInterfeszImpl.java
Author      : Tobik János
Version     : 1.0
Title		: Interfész
Description	: Interfész implementációja.
============================================================================
*/

package hu.tobikjanos.java.oop_3.interfesz;

public class EgyszeruInterfeszImpl implements EgyszeruInterfesz
{

    public String method0()
    {
        return "HELLO WORLD";
    }

    @Override
    public void method1()
    {
        System.out.println("Hello World");
    }

    @Override
    public int method2(String arg1)
    {
        System.out.println("arg1 = " + arg1);

        return this.SOME_VALUE;
    }
}
