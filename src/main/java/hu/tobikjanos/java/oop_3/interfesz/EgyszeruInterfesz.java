/*
============================================================================
Name        : EgyszeruInterfesz.java
Author      : Tobik János
Version     : 1.0
Title		: Interfész
Description	: Metódus és attribútum definíció.
============================================================================
*/

package hu.tobikjanos.java.oop_3.interfesz;

public interface EgyszeruInterfesz
{
    void method1();

    int method2(String arg1);

    public static final int SOME_VALUE = 100;   // automatikusan public static final lesz az attribútum
    int SOME_VALUE2 = 200;


    // egy kis trükk futtatható kód írására interfészben
    public class Foo
    {
        public static void bar()
        {
            System.out.println("Hello WORLD!!!!!!");
        }
    }
}
