/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: Interfész
Description	: Különbőző implementációk használata.
============================================================================
*/

package hu.tobikjanos.java.oop_3.interfesz;

public class Main
{

    public static void main(String[] args)
    {
        EgyszeruInterfesz egyszeruInterfesz = new EgyszeruInterfeszImpl();

//        egyszeruInterfesz.method0();
        egyszeruInterfesz.method1();
        egyszeruInterfesz.method2("Hello");

        EgyszeruInterfesz egyszeruInterfesz2 = new EgyszeruInterfeszImpl2();

//        egyszeruInterfesz.method0();
        egyszeruInterfesz.method1();
        egyszeruInterfesz.method2("Hello");

        EgyszeruInterfesz.Foo.bar();
    }
}
