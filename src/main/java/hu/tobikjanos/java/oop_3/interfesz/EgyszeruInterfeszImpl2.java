/*
============================================================================
Name        : EgyszeruInterfeszImpl2.java
Author      : Tobik János
Version     : 1.0
Title		: Interfész
Description	: Interfész implementációja.
============================================================================
*/

package hu.tobikjanos.java.oop_3.interfesz;

public class EgyszeruInterfeszImpl2 implements EgyszeruInterfesz
{

    public String method0()
    {
        return "HELLO WORLD 2";
    }

    @Override
    public void method1()
    {
        System.out.println("Hello World 2");
    }

    @Override
    public int method2(String arg1)
    {
        System.out.println("arg1 = " + arg1);

        return this.SOME_VALUE + 200;
    }
}
