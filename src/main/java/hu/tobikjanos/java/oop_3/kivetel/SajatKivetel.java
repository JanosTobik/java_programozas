/*
============================================================================
Name        : SajatKivetel.java
Author      : Tobik János
Version     : 1.0
Title		: Kivétel
Description	: Saját kivétel osztály létrehozása.
============================================================================
*/

package hu.tobikjanos.java.oop_3.kivetel;

public class SajatKivetel extends Exception
{
    public SajatKivetel(String message)
    {
        super(message);
    }
}
