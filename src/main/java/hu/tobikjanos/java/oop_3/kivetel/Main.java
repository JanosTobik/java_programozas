/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: Kivétel
Description	: Program futása során a kivételeket arra használjuk, hogy a nem megfelelő működésre figyelmeztessenek.
              Amikor egy kivételt "dob" a program, a normális működés megszakad és a program leáll.
              Kivételek kezelésére a try-catch szerkezetet alkalmazzuk.
============================================================================
*/

package hu.tobikjanos.java.oop_3.kivetel;

public class Main
{
    public static void main(String[] args)
    {
        try
        {
            method1();
        } catch (SajatKivetel sajatKivetel)
        {
            sajatKivetel.printStackTrace();
        }
        finally
        {
            System.out.println("Ez a blokk mindig lefut");
        }
    }

    private static void method1() throws SajatKivetel
    {
        if (10 < 15)
        {
            throw new SajatKivetel("Saját kivételt dob ez a metódus");
        }
    }
}
