/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		:
Description	:
============================================================================
*/

package hu.tobikjanos.java.oop_3.szarmaztatas;

public class Main {
    public static void main(String[] args)
    {
        OsOsztaly osOsztaly = new OsOsztaly(100, "Hello");
        osOsztaly.method1();
        osOsztaly.method2();

        GyerekOsztaly gyerekOsztaly = new GyerekOsztaly(30);
        gyerekOsztaly.method1();
        gyerekOsztaly.method2();
        gyerekOsztaly.method3();
    }
}
