/*
============================================================================
Name        : GyerekOsztaly.java
Author      : Tobik János
Version     : 1.0
Title		: Gyerek Osztaly
Description	:
============================================================================
*/

package hu.tobikjanos.java.oop_3.szarmaztatas;

public class GyerekOsztaly extends OsOsztaly {
    private Integer gyerekPrivatInt;

    public GyerekOsztaly(Integer gyerekPrivatInt) {
        super(12, "WORLD");
        this.gyerekPrivatInt = gyerekPrivatInt;
    }

    public void method3()
    {
        System.out.println("gyerekPrivatInt = " + gyerekPrivatInt);
    }
}
