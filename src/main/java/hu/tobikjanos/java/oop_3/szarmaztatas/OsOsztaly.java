/*
============================================================================
Name        : OsOsztaly.java
Author      : Tobik János
Version     : 1.0
Title		:
Description	:
============================================================================
*/

package hu.tobikjanos.java.oop_3.szarmaztatas;

public class OsOsztaly {
    private Integer osPrivatInt;
    protected String osProtectedStr;

    public OsOsztaly() {
    }

    public OsOsztaly(Integer osPrivatInt, String osProtectedStr) {
        this.osPrivatInt = osPrivatInt;
        this.osProtectedStr = osProtectedStr;
    }

    public void method1()
    {
        System.out.println("osPrivatInt = " + osPrivatInt);
    }

    public void method2()
    {
        System.out.println("osProtectedStr = " + osProtectedStr);
    }
}
