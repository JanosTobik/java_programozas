/*
============================================================================
Name        : EgyszeruOsztaly.java
Author      : Tobik János
Version     : 1.0
Title		: Egyszerű osztály
Description	:
============================================================================
*/

package hu.tobikjanos.java.oop_3.osztalyok;

public class Main {

    public static void main(String[] args)
    {
        // Objektum létrehozása (példányosítás)
        EgyszeruOsztaly egyszeruOsztaly = new EgyszeruOsztaly();

        // Példány attribútumainak megadása
        egyszeruOsztaly.myInteger = 10;
        egyszeruOsztaly.str = "Hello World";
        egyszeruOsztaly.character = 'R';

        // Tagfüggvények meghívása az objektumon
        egyszeruOsztaly.metodus1();
        egyszeruOsztaly.metodus2();
        egyszeruOsztaly.metodus3();

        // Új példány létrehozása paraméterezett konstruktorral
        EgyszeruOsztaly egyszeruOsztaly2 = new EgyszeruOsztaly("hello world!");
        System.out.println("egyszeruOsztaly2.str = " + egyszeruOsztaly2.str);

        egyszeruOsztaly2.metodus1();

        /*--------------------------------------------------------------------------------------------*/

        Hozzaferhetoseg.myStaticInt = 333;
    }
}
