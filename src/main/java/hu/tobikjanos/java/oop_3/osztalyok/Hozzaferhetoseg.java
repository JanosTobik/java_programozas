/*
============================================================================
Name        : Hozzaferhetoseg.java
Author      : Tobik János
Version     : 1.0
Title		: Hozzáférhetőség
Description	:
============================================================================
*/

package hu.tobikjanos.java.oop_3.osztalyok;

public abstract class Hozzaferhetoseg {
    // package      - package-en belül érhető el
    // private      - csak az osztályon belül érhető el
    // protected    - osztályon belül és a származtatott osztályokban érhető el
    // public       - bárhol elérhető
    int a;
    private Integer myInteger;
    protected String myString;
    public Integer pubInteger;


    // static       - osztályszintű attribútum vagy metódus
    // final        - változó esetén konstans definiálása, metódus esetén a származtatott osztály nem írhatja felül a metódust
    // abstract     - nincs metódus implementáció, de a származtatott osztálynak implementálnia kell; ha az osztályon belül egy metódus abstract, akkor az osztály is abstract (nem példányosítható)

    public static int myStaticInt = 5;

    // static inicializáló blokk, lefut ha az osztály betöltődik
    static {
        String myStaticString1 = "HELLO";
        String myStaticString2 = "WORLD";
    }

    private final Integer CONSTANT = 200;

    public void method() {

    }
}
