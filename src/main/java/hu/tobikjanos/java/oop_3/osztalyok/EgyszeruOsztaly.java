/*
============================================================================
Name        : EgyszeruOsztaly.java
Author      : Tobik János
Version     : 1.0
Title		: Egyszerű osztály
Description	:
============================================================================
*/

package hu.tobikjanos.java.oop_3.osztalyok;

public class EgyszeruOsztaly {

    ///////////////////
    // Osztály változói
    ///////////////////

    String str;
    Integer myInteger;
    char character;

    //////////////
    // Konstruktor
    //////////////
    // Akkor fut le, amikor az osztályból létrehozunk egy példányt (objektumot).

    public EgyszeruOsztaly() {
        System.out.println("Konstruktor - létrejött egy példány");
    }

    // Paraméterezett konstruktor.

    public EgyszeruOsztaly(String str) {
        this.str = str;
    }


    ////////////////
    // Tagfüggvények
    ////////////////

    void metodus1()
    {
        System.out.println("metodus1");
    }

    void metodus2()
    {
        System.out.println("metodus2");
    }

    void metodus3()
    {
        System.out.println("metodus3");
    }
}
