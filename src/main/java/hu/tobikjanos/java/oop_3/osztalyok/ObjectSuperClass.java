/*
============================================================================
Name        : ObjectSuperClass.java
Author      : Tobik János
Version     : 1.0
Title		: Object Super Class
Description	:
============================================================================
*/

package hu.tobikjanos.java.oop_3.osztalyok;

public class ObjectSuperClass
{
    // Metódusok

    // hash kód generálás
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    // egyenlőség vizsgálata az objektum attribútumainak alapján
    @Override
    public boolean equals(Object obj)
    {
        return super.equals(obj);
    }

    // objektum String értéke
    @Override
    public String toString()
    {
        return super.toString();
    }
}
