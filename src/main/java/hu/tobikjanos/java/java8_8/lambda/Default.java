/*
============================================================================
Name        : Default.java
Author      : Tobik János
Version     : 1.0
Title		: Lambda
Description	: Default funkcionális interfészek.
============================================================================
*/

package hu.tobikjanos.java.java8_8.lambda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Default
{
    public static void main(String[] args)
    {
        /**
         * Predicate: logikai értékkel tér vissza
         */
        Predicate<String> predicate1 = s -> s.length() < 3 || s.contains("e");
        System.out.println("predicate1.test(\"Hello\") = " + predicate1.test("Hello"));
        System.out.println("predicate1.test(\"Ab\") = " + predicate1.test("Ab"));

        Predicate<Integer> predicate2 = i -> i > 10 && i < 100;
        System.out.println("predicate2.negate().test(50) = " + predicate2.negate().test(50));
        System.out.println("predicate2.test(50) = " + predicate2.test(50));
        System.out.println("\n--------------------------------\n");


        /**
         * Function: paraméterezett művelet végrehajtása visszatérési értékkel
         */
        Function<String, Long> function1 = s -> Long.valueOf(s);
        System.out.println("function1.apply(\"20\") = " + function1.apply("20"));
        System.out.println("\n--------------------------------\n");


        /**
         * Supplier: paraméter nélküli művelet végrehajtása visszatérési értékkel
         */
        Supplier<List<String>> supplier1 = () -> Arrays.asList("A", "B", "C");
        System.out.println("supplier1.get() = " + supplier1.get());

        Supplier<byte[]> supplier2 = () -> "ABCDEF".getBytes();
        System.out.println("supplier2.get() = " + Arrays.toString(supplier2.get()));
        System.out.println("\n--------------------------------\n");


        /**
         * Consumer: paraméterezett művelet végrehajtása visszatérési érték nélkül
         */
        Consumer<Integer> consumer1 = i -> {
            i += 100;
            System.out.println("i = " + i);
        };
        consumer1.accept(15);
        System.out.println("\n--------------------------------\n");


        /**
         * Comparator
         */
        Comparator<String> comparator1 = (o1, o2) -> o1.compareTo(o2);
        System.out.println("comparator1.compare(\"W\", \"X\") = " + comparator1.compare("W", "X"));

    }
}
