/*
============================================================================
Name        : Lambda.java
Author      : Tobik János
Version     : 1.0
Title		: Lambda
Description	: Lambda kifejezések használata.
              Lambda függvény szintaxisa: (parameter) -> {expression body}
============================================================================
*/

package hu.tobikjanos.java.java8_8.lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lambda
{
    public static void main(String[] args)
    {
        /**
         * Lista rendezése saját Comparator osztály használata nélkül.
         */
        List<String> stringList = Arrays.asList("C", "X", "A", "F", "R", "L");
        stringList.sort((a, b) -> a.compareTo(b));
        stringList.sort(Comparator.naturalOrder());
        Collections.sort(stringList, (a, b) -> a.compareTo(b));
        Collections.sort(stringList, Comparator.reverseOrder());


        /**
         * Saját Functional Interface használata
         */
        MyFunctionalIFace<Integer, String> myFunctionalIFac1 = new MyFunctionalIFace<Integer, String>()
        {
            @Override
            public Integer apply(String p)
            {
                return Integer.valueOf(p);
            }
        };

        Integer myInt = myFunctionalIFac1.apply("1000");
        System.out.println("myInt = " + myInt);


        MyFunctionalIFace<String, Long> myFunctionalIFac2 = p -> p.toString();

        String myStr = myFunctionalIFac2.apply(30L);
        System.out.println("myStr = " + myStr);

    }

    @FunctionalInterface
    private interface MyFunctionalIFace<R, P>
    {
        R apply(P p);
    }
}
