/*
============================================================================
Name        : MethodReference.java
Author      : Tobik János
Version     : 1.0
Title		: Metódus referencia
Description	:
============================================================================
*/

package hu.tobikjanos.java.java8_8.methodreference;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class MethodReference
{
    public static void main(String[] args)
    {
        List<String> stringList = Arrays.asList("S", "A", "D", "F");
        stringList.forEach(System.out::println);
        System.out.println("\n--------------------------------\n");


        Function<String, Integer> function1 = String::length;
        System.out.println("function1.apply(\"10\") = " + function1.apply("Hello World!"));

        Function<String, Integer> function2 = Integer::valueOf;
        System.out.println("function2.apply(\"45\") = " + function2.apply("45"));

    }
}
