/*
============================================================================
Name        : Lambda.java
Author      : Tobik János
Version     : 1.0
Title		: OptionalObject
Description	:
============================================================================
*/

package hu.tobikjanos.java.java8_8.optional;

import java.util.Optional;

public class OptionalObject
{
    public static void main(String[] args)
    {
        Optional<String> empty = Optional.empty();
        try
        {
            System.out.println(empty.get());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /**
         * isPresent, ifPresent
         */
        Optional<String> optionalString = Optional.of("Hello World");
        System.out.println(optionalString.get());
        optionalString.ifPresent(string -> System.out.println("string = " + string));
        optionalString.ifPresent(System.out::println);
        if (optionalString.isPresent())
        {
            String string = optionalString.get();
            System.out.println(string);
        }

        System.out.println("--------------------------------------------------------------");

        /**
         * orElse, orElseThrow, orElseGet
         */
        String string1 = optionalString.orElse(null);
        String string2 = optionalString.orElseThrow(() -> new RuntimeException("Valami hiba történt"));
        String string3 = optionalString.orElseGet(() -> new String("hello world!!!"));

        System.out.println("--------------------------------------------------------------");

        /**
         * map
         */
        Optional<Integer> stringLength = optionalString.map(string -> string.length());
        System.out.println("stringLength.get() = " + stringLength.get());

        System.out.println("--------------------------------------------------------------");

        /**
         * nullable
         */
        String nullString = null;
        Optional<String> optionalNullString = Optional.ofNullable(nullString);
        optionalNullString.ifPresent(string -> System.out.println("string = " + string));

        System.out.println("--------------------------------------------------------------");
    }
}
