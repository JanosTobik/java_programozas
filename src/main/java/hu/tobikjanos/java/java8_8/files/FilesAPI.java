/*
============================================================================
Name        : FilesAPI.java
Author      : Tobik János
Version     : 1.0
Title		: Files api
Description	: Fájlkezelés java 8-ban.
============================================================================
*/

package hu.tobikjanos.java.java8_8.files;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FilesAPI
{
    public static void main(String[] args) throws IOException
    {
        byte[] bytes = Files.readAllBytes(Paths.get("src/main/resources/input.txt"));
        System.out.println("new String(bytes) = " + new String(bytes));

        Files.readAllLines(Paths.get("src/main/resources/input.txt")).forEach(System.out::println);

        boolean isDir = Files.isDirectory(Paths.get("src"));
        System.out.println("isDir = " + isDir);

        Files.write(Paths.get("src/main/resources/output2.txt"), "Hello World!".getBytes());

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream())
        {
            Files.copy(Paths.get("src/main/resources/input.txt"), baos);
            System.out.println("new String(baos.toByteArray()) = " + new String(baos.toByteArray()));
        }
    }
}
