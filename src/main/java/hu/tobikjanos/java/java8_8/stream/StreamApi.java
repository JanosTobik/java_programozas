/*
============================================================================
Name        : Lambda.java
Author      : Tobik János
Version     : 1.0
Title		: StreamApi
Description	: Stream api használata.
============================================================================
*/

package hu.tobikjanos.java.java8_8.stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.OptionalLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamApi
{
    private static List<Long> numbers = new ArrayList<>();
    private static List<Item> items = new ArrayList<>();
    private static Long sum = 0L;

    static {
        numbers.add(3L);
        numbers.add(7L);
        numbers.add(2L);
        numbers.add(11L);
        numbers.add(6L);

        items.add(new Item(15L, "name1", 100));
        items.add(new Item(22L, "name2", 33));
        items.add(new Item(13L, "name3", 0));
        items.add(new Item(46L, "name4", 100));
        items.add(new Item(59L, "name5", 231));
    }

    public static void main(String[] args)
    {
        numbers.forEach(n -> sum += n);
        System.out.println("sum = " + sum);
        sum = 0L;
        numbers.stream().forEach(n -> sum += n);
        System.out.println("sum = " + sum);

        System.out.println("--------------------------------------------------------------");

        /**
         * Stream segítségével lista létrehozása
         */
        List<Integer> integers = Stream.iterate(0, i -> i + 3).limit(10).collect(Collectors.toList());
        integers.forEach(i -> System.out.println("i = " + i));

        List<Long> longs = Stream.of(1L, 22L, 76L, 0L).collect(Collectors.toList());
        longs.forEach(l -> System.out.println("l = " + l));

        System.out.println("--------------------------------------------------------------");

        /**
         * rendezés
         */
        items.stream()
                .sorted((x1, x2) -> x1.getValue().compareTo(x2.getValue()))
                .forEach(value -> System.out.println("value = " + value));

        items.stream()
                .sorted(Comparator.comparing(Item::getValue))
                .forEach(value -> System.out.println("value = " + value));

        items.stream()
                .sorted(Comparator.comparing(Item::getValue).reversed())
                .forEach(value -> System.out.println("value = " + value));

        items.stream()
                .sorted(Comparator.comparing(Item::getValue).thenComparing(Item::getName))
                .forEach(value -> System.out.println("value = " + value));

        System.out.println("--------------------------------------------------------------");

        /**
         * filterezés
         */
        numbers.stream().filter(n -> n > 5L).forEach(n -> System.out.println("n = " + n));

        List<Item> name2List = items.stream().filter(item -> item.getName().equals("name2")).collect(Collectors.toList());

        System.out.println("--------------------------------------------------------------");

        /**
         * findFirst, findAny, ...
         */
        Optional<Item> optionalItem = items.stream().filter(item -> item.getId() == 1L).findFirst();
        optionalItem.ifPresent(item -> System.out.println("item = " + item));
        if (optionalItem.isPresent())
        {
            Item item = optionalItem.get();
            System.out.println("item = " + item);
        }

        Optional<Item> optionalAny = items.stream().filter(item -> item.getValue() == 100L).findAny();
        optionalAny.ifPresent(item -> System.out.println("item = " + item));

        System.out.println("--------------------------------------------------------------");

        /**
         * aggregáció (max, min, average, sum)
         */
        OptionalLong max = numbers.stream().mapToLong(Long::longValue).max();

        OptionalInt min = items.stream().mapToInt(Item::getValue).min();

        OptionalDouble average = items.stream().mapToInt(Item::getValue).average();

        int sum = numbers.stream().mapToInt(Long::intValue).sum();

        System.out.println("--------------------------------------------------------------");

        /**
         * collect
         */
        String joined1 = numbers.stream().map(String::valueOf).collect(Collectors.joining(", "));
        System.out.println("joined1 = " + joined1);

        String joined2 = items.stream().map(Item::getName).collect(Collectors.joining(", "));
        System.out.println("joined2 = " + joined2);

    }

    private static class Item
    {
        private Long id;
        private String name;
        private Integer value;

        public Item(Long id, String name, Integer value)
        {
            this.id = id;
            this.name = name;
            this.value = value;
        }

        public Long getId()
        {
            return id;
        }

        public void setId(Long id)
        {
            this.id = id;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public Integer getValue()
        {
            return value;
        }

        public void setValue(Integer value)
        {
            this.value = value;
        }

        @Override
        public String toString()
        {
            final StringBuilder sb = new StringBuilder("Item{");
            sb.append("id=").append(id);
            sb.append(", name='").append(name).append('\'');
            sb.append(", value=").append(value);
            sb.append('}');
            return sb.toString();
        }
    }
}
