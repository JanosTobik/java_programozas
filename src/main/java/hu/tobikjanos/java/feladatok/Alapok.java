/*
============================================================================
Name        : Alapok.java
Author      : Tobik János
Version     : 1.0
Title		: Alap feladatok
Description	:
============================================================================
*/

package hu.tobikjanos.java.feladatok;

public class Alapok
{
    public static void main(String[] args)
    {
        /**
         * 1. Feladat - Bevezetés (változók, operátorok)
         */

        int a = 5;
        int b = 2;
        int c = 10;

        // 1.1 - Hozz létre egész szám típusú 'x', 'y' és 'z' nevű változókat.

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/


        // 1.2 - 'x' változó értéke legyen az 'a' változó negyedik hatványa osztva 3-mal.
        //     - 'y' változó értéke legyen 'b' változó háromszorosának és 'c' változó kétszeresének különbsége
        //     - 'z' változó értéke legyen az előbb kiszámolt 'x' és 'y' összege

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/


        // 1.3 - Egy új változó 'ab' névvel tárolja el az 'a' és 'b' változók szorzatát.
        //     - Egy új változó 'bc' névvel tárolja el a 'b' és 'c' változók szorzatát.
        //     - Egy új változó 'ac' névvel tárolja el az 'a' és 'c' változók szorzatát.

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/


        // 1.4 - 'x' új értéke legyen az 'ab', 'bc' és 'ac' összege.
        //     - 'y' új értéke legyen az 'ab', 'bc' és 'ac' szorzata.
        //     - 'z' legyen az 'ab' és 'ac' különbsége.

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/



        /**
         * 2. Feladat - vezérlési szerkezetek (logikai kifejezések, elágazások, ciklusok)
         */

        // 2.1 - Előző feladatból származó 'x' értékéről döntsük el, hogy pozitív vagy negatív. Ha pozitív írjuk ki a standard outputra "POZITIV", különben írjuk ki, hogy "NEGATIV".
        //     - Standard outputra való kiíráshoz a System.out.println("VALAMI SZOVEG"); parancsot használjuk.
        //     - 'y' értékéről döntsük el, hogy páros-e. Ha páros írjuk ki, hogy "PAROS", ha páratlan írjuk ki, hogy "PARATLAN". (segítség: megoldáshoz használjuk a maradékos osztás operátort)

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/


        // 2.2 - Vizsgáljuk meg 'x' értékét. Ha 25 és 50 között van írjuk ki "25-50", ha 51 és 80 között van írjuk ki "51-80, egyébként írjuk ki "A szam nincs a vizsgalt tartomanyban!"

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/


        // 2.3 - Hozzunk létre egy 10 elemű egész számokat tároló tömböt. Töltsük fel a következő adatokkal: 31, 2, 64, 1111, -79, 42, 5, 57, 90, 200
        //     - Ciklus és elágazás segítségével számoljuk meg hány páros számot tartalmaz a tömb.
        //     - Az eredményt írjuk ki a standard outputra.

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/


        // 2.4 - A 2.3-ban létrehozott tömb elemei közül írjuk ki az első 5 elemet.
        //     - A 2.3-ban létrehozott tömb elemei közül írjuk ki az utolsó 3 elemet. (segítség: egy ciklus nem csak az első elemtől az utolsóig mehet, hanem fordítva is)

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/


        // 2.5 - A 2.3-ban létrehozott tömbben ciklus és elágazás segítségével keressük meg a legkisebb elemet a tömbben.
        //     - A standard outputra írjuk ki a legkisebb elem értékét és a tömbben elfoglalt helyét (indexét).

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/




        // 2.6 - A 2.3-ban létrehozott tömböt rendezzük csökkenő sorrendbe.
        //     - A standard outputra írjuk ki a rendezzett tömb elemeit.

        /*-----------  MEGOLDÁS IDE  -----------*/




        /*--------------------------------------*/

    }

    /**
     * 3. Feladat - metódusok
     */

    // 3.1 - Írj metódust, amely 5-ször kiírja a standard outputra a következő szöveget: "HeLLo WoRLD!!!".

    /*-----------  MEGOLDÁS IDE  -----------*/




    /*--------------------------------------*/

    // 3.2 - Írj metódust, amely eldönti két egész típusú számról, hogy melyik a nagyobb és visszatér azzal.

    /*-----------  MEGOLDÁS IDE  -----------*/




    /*--------------------------------------*/


    // 3.3 - Írj metódust, amely visszaadja egy double típusú szám négyzetét.

    /*-----------  MEGOLDÁS IDE  -----------*/




    /*--------------------------------------*/


    // 3.4 - Írj metódust, amely eldönti egy egész típusú számról, hogy osztható-e 7-el. Visszatérési értéke legyen logikai igaz vagy hamis (boolean).

    /*-----------  MEGOLDÁS IDE  -----------*/




    /*--------------------------------------*/
}
