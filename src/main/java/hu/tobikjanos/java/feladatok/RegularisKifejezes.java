package hu.tobikjanos.java.feladatok;

/**
 * Segítség reguláris kifejezésekhez:
 *      https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
 */

public class RegularisKifejezes
{
    public static void main(String[] args)
    {
        String minta, szo1, szo2, szo3;

        /**
         * Bevezetés
         */

        // 1. feladat:
        // Írj olyan reguláris kifejezést, amely elfogadja az 'X' karaktert.
        System.out.println("------------------------------- 1. Feladat -------------------------------");
        minta = "";
        szo1 = "X";
        szo2 = "x";
        szo3 = "xyxy";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.FALSE, szo3.matches(minta));


        // 2. feladat:
        // Írj olyan reguláris kifejezést, amely elfogad egy darab bármilyen karaktert az 'x' karakter után.
        System.out.println("------------------------------- 2. Feladat -------------------------------");
        minta = "";
        szo1 = "xy";
        szo2 = "xaabed";
        szo3 = "x5";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.TRUE, szo3.matches(minta));

        // 3. feladat:
        // Írj olyan reguláris kifejezést, amely elfogad bármennyi 'a' karaktert és utána bármennyi 'b' karaktert.
        System.out.println("------------------------------- 3. Feladat -------------------------------");
        minta = "";
        szo1 = "aaaaaaabbb";
        szo2 = "aaaacbbbbbb";
        szo3 = "aaabbccccc";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.FALSE, szo3.matches(minta));


        // 4. feladat:
        // Írj olyan reguláris kifejezést, amely elfogad egy vagy több szót szóközzel elválasztva.
        // A szavak tartalmazhatnak kis és nagy betűket 'a'-tól 'z'-ig.
        System.out.println("------------------------------- 4. Feladat -------------------------------");
        minta = "";
        szo1 = "Hello World";
        szo2 = "Hello World!!!";
        szo3 = "My dog says meow";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.TRUE, szo3.matches(minta));


        // 5. feladat:
        // Írj olyan reguláris kifejezést, amely elfogad egy vagy több szót szóközzel elválasztva.
        // A szavak tartalmazhatnak kis és nagy betűket 'a'-tól 'z'-ig, illetve számokat.
        System.out.println("------------------------------- 5. Feladat -------------------------------");
        minta = "";
        szo1 = "Hello 101 World";
        szo2 = "Hello World 300$";
        szo3 = "Hello WORLD 000";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.TRUE, szo3.matches(minta));


        // 6. feladat:
        // Írj olyan reguláris kifejezést, amely elfogadja a 'dog', 'cat', 'cow' szavak valamelyikét.
        System.out.println("------------------------------- 6. Feladat -------------------------------");
        minta = "";
        szo1 = "dog";
        szo2 = "cat";
        szo3 = "bird";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.TRUE, szo2.matches(minta));
        printResult(Boolean.FALSE, szo3.matches(minta));


        // 7. feladat:
        // Írj olyan reguláris kifejezést, amely elfogad egy négy betűs szót.
        System.out.println("------------------------------- 7. Feladat -------------------------------");
        minta = "";
        szo1 = "THIS";
        szo2 = "these";
        szo3 = "test";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.TRUE, szo3.matches(minta));


        // 8. feladat:
        // Írj olyan reguláris kifejezést, amely elfogadja a 100 és 999999 közötti számokat.
        System.out.println("------------------------------- 8. Feladat -------------------------------");
        minta = "";
        szo1 = "100";
        szo2 = "34";
        szo3 = "992266";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.TRUE, szo3.matches(minta));


        /**
         * Nehezebb feladatok
         */


        // 9. feladat:
        // Írj olyan reguláris kifejezést, amely elfogadja a 'docx' kiterjesztésű fájlneveket.
        // A kiterjesztés előtti szó kis vagy nagy betűvel kell kezdődjön és tartalmazhatja a következő speciális karaktereket:
        // '_', '-', '$', '&', '#', '@', ' '
        System.out.println("------------------------------- 9. Feladat -------------------------------");
        minta = "";
        szo1 = "valamiNev.docx";
        szo2 = "Ez @ e-g-y $ docx ## fajl.docx";
        szo3 = "valamiNev.txt";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.TRUE, szo2.matches(minta));
        printResult(Boolean.FALSE, szo3.matches(minta));

        // 10. feladat:
        // Írj olyan reguláris kifejezést, amely elfogadja az e-mail címeket.
        // A '@' előtti név kis vagy nagy betűvel kell kezdődjön, utána minimum 3, maximum 20 darab bármilyen karakter szerepelhet.
        // A '@' után a '.' karakterig 'a'-tól 'z'-ig bármilyen kis és nagy betű lehet, de minimum 4 darabnak lennie kell.
        // A '.' után minimum 2, maximum 3 darab karakter lehet 'a'-tól 'z'-ig.
        System.out.println("------------------------------- 10. Feladat -------------------------------");
        minta = "";
        szo1 = "janostobik@gmail.com";
        szo2 = "$qwerty@hotmail.com";
        szo3 = "A###hh@gmail.com";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.FALSE, szo2.matches(minta));
        printResult(Boolean.TRUE, szo3.matches(minta));

        // 11. feladat:
        // Írj olyan reguláris kifejezést, amely elfogadja a magyar mobil telefonszámokat.
        // Formátum: +36001234567 -> +36-al kezdődik, utána kettő darab szám, amely lehet '20', '30' vagy '70', utána 7 darab tetszőleges szám,
        System.out.println("------------------------------- 11. Feladat -------------------------------");
        minta = "";
        szo1 = "+36201234567";
        szo2 = "+36301234567";
        szo3 = "+367012345678";
        printResult(Boolean.TRUE, szo1.matches(minta));
        printResult(Boolean.TRUE, szo2.matches(minta));
        printResult(Boolean.FALSE, szo3.matches(minta));


        /**
         * Extra feladat
         */

        // 12. feladat:
        // Írd meg az 'isPhoneNumberValid' metódust úgy, hogy elfogadja a magyar mobil telefonszámokat különböző formátumokban.
        // A megoldáshoz bármilyen String művelet, reguláris kifejezés, vezérlési szerkezet használható. (lényegében bármi :) )
        // Elfogadható formátumok:
        // - 0036201234567 -> elején 2 darab '0', utána '36', utána '20', '30' vagy '70'
        // - +36201234567 -> előző feladat
        // - +36-20-1234567 -> kötőjelekkel elválasztott, a '+' karakter helyett lehet '00' is (pl: 0036-20-1234567)
        // - +36-20-12-34-567
        // - +36-20-123-45-67
        // - +36-20-123-4567
        System.out.println("------------------------------- 12. Feladat -------------------------------");
        printResult(Boolean.TRUE, isPhoneNumberValid("0036201234567"));
        printResult(Boolean.TRUE, isPhoneNumberValid("+36301234567"));
        printResult(Boolean.TRUE, isPhoneNumberValid("+36-70-1234567") && isPhoneNumberValid("0036-70-1234567"));
        printResult(Boolean.TRUE, isPhoneNumberValid("+36-20-12-34-567") && isPhoneNumberValid("0036-20-12-34-567"));
        printResult(Boolean.TRUE, isPhoneNumberValid("+36-20-123-45-67") && isPhoneNumberValid("0036-20-123-45-67"));
        printResult(Boolean.TRUE, isPhoneNumberValid("+36-20-123-4567") && isPhoneNumberValid("0036-20-123-4567"));
    }

    private static Boolean isPhoneNumberValid(String phoneNumber)
    {
        return false;
    }

    private static void printResult(Boolean expected, Boolean actualValue)
    {
        System.out.println("Várt érték:\t" + expected + "\tkapott eredmény:\t" + actualValue);
    }

































    /*
    Megoldások:
        1. feladat: X
        2. feladat: x.
        3. feladat: a*b*
        4. feladat: [a-zA-Z ]*
        5. feladat: [a-zA-Z 0-9]*
        6. feladat: dog|cat|cow
        7. feladat: [a-zA-z]{4}
        8. feladat: \d{3,6}
        9. feladat: [A-Za-z][A-Za-z_\-$&#@ ]*\.docx
        10. feladat: [a-zA-Z].{3,20}@[a-zA-Z]{4,}\.[a-z]{2,3}
        11. feladat: \+36[237]0\d{7} vagy \+36(2|3|7)0\d{7} vagy \+36(20|30|70)\d{7}

        12. feladat:
        private static Boolean isPhoneNumberValid(String phoneNumber)
        {
            phoneNumber = phoneNumber.replaceAll("\\D", "");
            phoneNumber = phoneNumber.startsWith("00") ? phoneNumber.substring(2) : phoneNumber;
            return phoneNumber.matches("36(20|30|70)\\d{7}");
        }
     */
}
