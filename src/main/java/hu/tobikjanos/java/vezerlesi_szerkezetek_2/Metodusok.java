/*
============================================================================
Name        : Metodusok.java
Author      : Tobik János
Version     : 1.0
Title		: Metódusok (/függvények)
Description	:
============================================================================
*/

package hu.tobikjanos.java.vezerlesi_szerkezetek_2;

/**
 * Metódusokba/függvényekbe rendezzük a programkód bizonyos kódrészleteit.
 * Előnye, hogy egy kódrészletet többször is felhasználhatunk a programunkban és egyetlen paranccsal hivatkozhatunk rá.
 * Ezáltal könnyebben értelmezhető, olvasható programkódot kapunk, programunk moduláris felépítésű lesz.
 * Egy metódus kaphat paramétereket, amelyekkel műveleteket hajt végre, visszatérhet valamilyen értékkel.
 * Szintaktika:
 *      <visszatérési érték> <metódus neve> (<paraméterek>)
 *      {
 *          <metódus blokk>
 *      }
 */

public class Metodusok
{
    public static void main(String[] args)
    {
        hello();
        hello();
        hello();
        System.out.println("-----------------------------------------------------");

        // ----------------------------------------------------------------------

        int x = 13;
        int y = 5;


        int osszeg1 = osszead(x, y);
        int osszeg2 = osszead(y, x);
        int osszeg3 = osszead(x, 21);
        int osszeg4 = osszead(x, x);

        System.out.println("osszeg1 = " + osszeg1);
        System.out.println("osszeg2 = " + osszeg2);
        System.out.println("osszeg3 = " + osszeg3);
        System.out.println("osszeg4 = " + osszeg4);
        System.out.println("-----------------------------------------------------");

        // ----------------------------------------------------------------------


        int minimum1 = min(x, y);
        int minimum2 = min(y, x);
        int minimum3 = min(111, 64);

        System.out.println("minimum1 = " + minimum1);
        System.out.println("minimum2 = " + minimum2);
        System.out.println("minimum3 = " + minimum3);
        System.out.println("-----------------------------------------------------");

        // ----------------------------------------------------------------------

        int[] intTomb1 = { 65, 100, 0, -54 };
        int[] intTomb2 = { 42, -110, 4, 687 };

        tombKiir(intTomb1);
        System.out.println();
        tombKiir(intTomb2);
        System.out.println("-----------------------------------------------------");

        // ----------------------------------------------------------------------

        int[] tomb = tombElsoKetEleme(intTomb1);
        tombKiir(tomb);

    }

    /**
     * Metódus neve: hello
     * Visszatérési érték típusa: void (nincs visszatérési érték)
     * Paraméterek: nincs paraméter
     */
    static void hello()
    {
        System.out.println("Hello World!");
    }


    /**
     * Metódus neve: osszead
     * Visszatérési érték típusa: int
     * Paraméterek: két darab int típusú változó
     */
    static int osszead(int a, int b)
    {
        return a + b;
    }


    /**
     * Metódus neve: min
     * Visszatérési érték típusa: int
     * Paraméterek: két darab int típusú változó
     */
    static int min(int a, int b)
    {
        if (a < b)
        {
            return a;
        }
        else
        {
            return b;
        }
    }


    /**
     * Metódus neve: tombKiir
     * Visszatérési érték típusa: void
     * Paraméterek: int típusú tömb
     */
    static void tombKiir(int[] tomb)
    {
        for (int elem : tomb)
        {
            System.out.println(elem);
        }
    }


    /**
     * Metódus neve: tombElsoKetEleme
     * Visszatérési érték: int típusú tömb
     * Paraméterek: int típusú tömb
     */
    static int[] tombElsoKetEleme(int[] tomb)
    {
        int[] eredmenyTomb = { tomb[0], tomb[1] };
        return eredmenyTomb;
    }
}

