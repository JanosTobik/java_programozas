/*
============================================================================
Name        : Elagazasok.java
Author      : Tobik János
Version     : 1.0
Title		: Elágazások
Description	:
============================================================================
*/

package hu.tobikjanos.java.vezerlesi_szerkezetek_2;

public class Elagazasok {

    public static void main(String[] args)
    {
        ///////////////////////////////
        // egyszerű logikai kifejezések
        ///////////////////////////////
        /*
        a == b		=>	igaz, ha 'a' és 'b' értéke egyenlő, különben hamis
        a != b		=>	igaz, ha 'a' és 'b' értéke különbözik, különben hamis
        a < b		=>	igaz, ha 'a' értéke kisebb, mint 'b' értéke, különben hamis
        a > b		=>	igaz, ha 'a' értéke nagyobb, mint 'b' értéke, különben hamis
        a <= b		=>	igaz, ha 'a' értéke kisebb vagy egyenlő, mint 'b' értéke , különben hamis
        a >= b		=>	igaz, ha 'a' értéke nagyobb vagy egyenlő, mint 'b' értéke , különben hamis
        */

        ////////////////////////////////
        // összetett logikai kifejezések
        ////////////////////////////////
        /*
        (kifejezes1) && (kifejezes2)		=> ÉS
        (kifejezes1) || (kifejezes2)		=> VAGY
        (kifejezes1) ^ (kifejezes2)			=> XOR (kizáró vagy)
        !(kifejezes1)						=> TAGADÁS

        Pl: ((a>b)&&(b!=3)) || ( !((c>2)||(c<3)) )

        A műveleti sorrend miatt néhány zárójel elhagyható, így leegyszerűsíthető a logikai kifejezés:	(a>b && b!=3) || !(c>2 || c<3)
        */

        ///////////////////////
        // elágazás létrehozása
        ///////////////////////
        // Maradékos osztás '%' jel segítségével.
        // Pl: 5%3 megadja az 5 3-mal való osztási maradékát.

        int a = 384;
        if (a%2 == 0)	// páros-e a szám
        {
            // ha páros a szám növelje az 'a' változó értékét 5-tel
            a+=5;
        }

        ////////////////////////
        // ha ..., egyébként ...
        ////////////////////////
        if (a%2 == 0)
        {
            a+=5;
        }
        else
        {
            a-=12;
        }


        ///////////////////////////
        // ha ..., egyébként ha ...
        ///////////////////////////
        // Egyszerűsítés: A blokk elhagyható ha 1 utasításból áll. (DE NEM AJÁNLOTT!)

        if (a%2 == 0)
            a+=5;
        else if(a%3 == 0)
            a*=2;
        else
            a-=12;

        ////////////////////////////////////////
        // elágazás ternary (hármas) operátorral
        ////////////////////////////////////////
        // Szintaxis: (kifejezés) ? (eredmény ha igaz) : (eredmény ha hamis)

        int b = a>=5 ? 100 : 0;


        //////////////
        // switch-case
        //////////////
        // A break parancs kötelező, ha az egyes esetek során a megfelelő blokk hajtódjon végre.

        String g = "5";

        switch (g)
        {
            case "1":
                System.out.println("g = " + g);
                break;
            case "2":
                System.out.println("g = " + g);
                break;
            case "3":
                System.out.println("g = " + g);
                break;
            case "4":
                System.out.println("g = " + g);
                break;
            case "5":
                System.out.println("g = " + g);
                break;
            default:
                System.out.println("g értéke nem 1 és 5 között van");
        }
    }

}
