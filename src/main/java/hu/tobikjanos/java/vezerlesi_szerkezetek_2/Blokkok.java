/*
============================================================================
Name        : Blokkok.java
Author      : Tobik János
Version     : 1.0
Title		: Blokkok
Description	:
============================================================================
*/

package hu.tobikjanos.java.vezerlesi_szerkezetek_2;

public class Blokkok {

    public static void main(String[] args)
    {
        ////////////////////
        // blokk létrehozása
        ////////////////////
        // Egy blokk {-tól }-ig tart.
        {
            System.out.println("Ez kiírás egy blokkon belül van");
        }

        ////////////////////////////
        // Egymásba ágyazott blokkok
        ////////////////////////////
        {
            System.out.println("Külső blokk");
            {
                System.out.println("Belső blokk");
            }
        }

        //////////////////////////////////
        // Blokkok és változók élettartama
        //////////////////////////////////
        int a = 3;
        {
            a = 5;
            System.out.println("a = " + a);
            {
                int b = 10;
                System.out.println("b = " + b);
            }
            // System.out.println("b = " + b);              ez a sor fordítási hibát okoz, mert a 'b' változó csak a belső blokkban létezik
        }
        System.out.println("a = " + a);

    }
}
