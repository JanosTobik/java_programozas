/*
============================================================================
Name        : Ciklusok.java
Author      : Tobik János
Version     : 1.0
Title		: Ciklusok
Description	:
============================================================================
*/

package hu.tobikjanos.java.vezerlesi_szerkezetek_2;

public class Ciklusok {

    public static void main(String[] args)
    {
        ///////////////
        // while ciklus
        ///////////////

        int a = 0;
        while (a < 5)		// amíg 'a' kisebb mint 5
        {
            System.out.println("a = " + a);
            a++;
        }

        //////////////////
        // do-while ciklus
        //////////////////
        // A while ciklus elöltesztelős. A hátultesztelős változatához a 'do' kulcsszót kell használnunk.
        // Ebben az esetben a ciklusmag a feltételtől függetlenül egyszer biztosan végrehajtódik.

        int c = 6;
        do
        {
            System.out.println("c = " + c);
            c++;
        } while(c <= 10);

        /////////////
        // for ciklus
        /////////////
        // Elöltesztelős ciklus, amelyhez ciklusváltozót kell használni.
        // A szintaktika tartalmazza a ciklusváltozó inicializálását és módosítását, valamint a bentmaradási feltételt.
        // Jól használható tömb/lista indexeléséhez, nem a tömb/lista aktuális elemének értéket kapja meg az index változó.

        for (int i=0; i<10; i++)
        {
            System.out.println("i = " + i);
        }

        /////////////////
        // foreach ciklus
        /////////////////
        // Működése hasonló a for ciklushoz.
        // Tömbök és listák esetén használható.
        // Végigfut a teljes tömbön/listán.
        // A ciklusváltozó a tömb/lista aktuális elemét kapja meg értékül és nem egy index változó lesz.

        int tomb[] = { 2, 5, 7, 1, 9 };
        for (int elem : tomb)
        {
            System.out.println("elem = " + elem);
        }

        ////////////////////
        // continue és break
        ////////////////////
        // A continue parancs hatására a ciklus továbblép a következő iterációra.
        // A break parancs hatására a ciklus további futását akadályozzuk meg.

        for (int i = 0; i < 10; i++)
        {
            if (i == 2)
            {
                i+=3;
                continue;
            }

            if (i == 8)
            {
                break;
            }

            System.out.println("i = " + i);
        }
    }

}
