/*
============================================================================
Name        : ProgramozasiTetelek.java
Author      : Tobik János
Version     : 1.0
Title		: Programozási tételek
Description	:
============================================================================
*/

package hu.tobikjanos.java.vezerlesi_szerkezetek_2;

public class ProgramozasiTetelek
{

    public static void main(String[] args)
    {

        /**
         * CSERE - Két változó értékének megcserélése.
         *
         * Megoldás elve: Egy új ideiglenes (segéd) változó bevezetése, amely eltárolja az egyik változó értékét.
         */
        System.out.println("-------------------------------");
        System.out.println("CSERE");

        int x;
        int y;

        x = 2;
        y = 10;

        int seged;

        seged = x;
        x = y;
        y = seged;

        System.out.println("x = " + x); // 10
        System.out.println("y = " + y); // 2



        /**
         * MEGSZÁMLÁLÁS - Egy adott szám megszámlálása, hogy hányszor szerepel a tömbben.
         *
         * Megoldás elve: Egy új változó bevezetése számláló céljából, amely kezdeti értéke 0.
         *                Végig kell menni a tömbön és ha az adott elem teljesíti a feltételt, akkor növeljük a számláló értékét 1-el.
         */
        System.out.println("-------------------------------");
        System.out.println("MEGSZAMLALAS");

        int tomb[] = {1, 2, 3, 4, 2, 8, 3};

        int szamlalo = 0;
        for (int i : tomb)
        {
            if (i == 2)
            {
                szamlalo++;
            }
        }

        System.out.println("Szamlalo erteke: " + szamlalo);



        /**
         * LINEÁRIS KERESÉS - Egy adott szám szerepel-e a tömb elemei között.
         *
         * Megoldás elve: Végig kell menni a tömbön és ha az adott elem megegyezik a keresett értékkel, akkor leállhatunk a kereséssel.
         */
        System.out.println("-------------------------------");
        System.out.println("LINEARIS KERESES");

        int keresettElem = 3;

        // ---------------------------------------------
        // 1. megoldás - Egyszerű keresés, nem tudjuk a keresett elem helyét (indexét).

        boolean megtalalva = false;
        for (int i : tomb)
        {
            if (i == keresettElem)
            {
                megtalalva = true;
                break;
            }
        }

        if (megtalalva)
        {
            System.out.println("A keresett elem szerepel a tomben.");
        }
        else
        {
            System.out.println("A keresett elem nem szerepel a tomben.");
        }


        // ---------------------------------------------
        // 2. megoldás - Meg tudjuk határozni a legelső előfordulási helyét a keresett elemnek.

        int idx = 0;
        while (idx < tomb.length && tomb[idx] != keresettElem)  // a ciklus addig megy amíg a tömb végére nem ér vagy meg nem találja a keresett elemet
        {
            idx++;
        }

        if (idx < tomb.length)
        {
            System.out.println("A keresett elem szerepel a tomben. Helye: " + idx);
        }
        else
        {
            System.out.println("A keresett elem nem szerepel a tomben.");
        }



        /**
         * MAXIMUM KERESÉS - Egy tömb elemei közül a legnagyobb értékű megtalálása.
         *
         * Megoldás elve: Egy új változó bevezetése, amely eltárolja a legnagyobb értéket.
         *                Ideiglenesen a tömb legelső elemét tekintjük a maximális értékűnek.
         *                Végig kell menni a tömbön és ha a következő elem nagyobb értékű, mint az eddigi maximum,
         *                akkor megváltoztatjuk a maximumot tároló változó értékét az új értékkel.
         */
        System.out.println("-------------------------------");
        System.out.println("MAXIMUM KERESES");


        // ---------------------------------------------
        // 1. megoldás - Egyszerű keresés, nem tudjuk a keresett elem helyét (indexét).

        int max = tomb[0];

        for (int i = 1; i < tomb.length; i++)   // kezdetben a legelső elemet tekintjük a legnagyobbnak, ezért a ciklust elég a második (i=1) elemtől kezdeni
        {
            if (tomb[i] > max)
            {
                max = tomb[i];
            }
        }
        System.out.println("Maximum ertek: " + max);


        // ---------------------------------------------
        // 2. megoldás - Meg tudjuk határozni a legelső előfordulási helyét a keresett elemnek.

        int maxIdx = 0;
        for (int i = 1; i < tomb.length; i++)
        {
            if (tomb[i] > tomb[maxIdx])
            {
                maxIdx = i;
            }
        }
        System.out.println("Maximum ertek: " + tomb[maxIdx] + " Helye: " + maxIdx);

        /**
         * RENDEZÉS - A tömb elemeinek növekvő sorrendbe való rendezése.
         *
         * Megoldás elve: Korábbi tételek alkalmazása.
         *                Tömb legkisebb elemének megkeresése.
         *                Ezt meg kell cserélni az első elemmel.
         *                Maradék közül a legkisebb elem megkeresése.
         *                Ezt cseréljük a második elemmel.
         */
        System.out.println("-------------------------------");
        System.out.println("RENDEZES");

        for (int i = 0; i < tomb.length - 1; i++)       // fő ciklus, amely aktuális elemét hasonlítjuk a tömb többi eleméhez
        {                                               // a ciklusnak elegendő az, hogy csak az utolsó előtti elemig megy, mert végül az utolsó elem a legnagyobb lesz, így felesleges tovább vizsgálni
            // minimum keresés
            int minIdx = i;                             // az aktuális elemet tekintjük kezdeti legkisebb elemnek
            for (int j = i + 1; j < tomb.length; j++)   // belső ciklus a minimum kereséshez a maradék elem között, a maradék elem a fő ciklus aktuális eleme utáni értékek
            {
                if (tomb[j] < tomb[minIdx])
                {
                    minIdx = j;
                }
            }
            // csere
            if (minIdx != i)                            // ha eleve ott van az elem akkor feleslegesen nem cserélünk
            {
                int csereValtozo = tomb[i];
                tomb[i] = tomb[minIdx];
                tomb[minIdx] = csereValtozo;
            }
        }

        for (int i = 0; i < tomb.length; i++)
        {
            System.out.println(i + ". => " + tomb[i]);
        }
    }
}
