/*
============================================================================
Name        : Bevezetes.java
Author      : Tobik János
Version     : 1.0
Title		: Bevezetés
Description	:
============================================================================
*/

package hu.tobikjanos.java.bevezetes_1;

public class Bevezetes {

    public static void main(String[] args)
    {
        System.out.println("Hello World!");
    }

}
