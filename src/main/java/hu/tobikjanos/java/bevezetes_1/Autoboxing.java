/*
============================================================================
Name        : Autoboxing.java
Author      : Tobik János
Version     : 1.0
Title		: Autoboxing
Description	:
============================================================================
*/

package hu.tobikjanos.java.bevezetes_1;

public class Autoboxing {

    public static void main(String[] args)
    {
        /////////////
        // Autoboxing
        /////////////
        // Minden primitív típusnak van osztály implementációja.
        // Például az 'int' típusnak van az 'Integer' osztály.
        // Előfordulhat olyan eset, hogy a primitív típust kell konvertálni az osztálybeli megfelelőjébe.
        // Szerencsére ezt a Java automatikusan kezeli.
        // Ezt a folyamatot Autoboxing-nak nevezzük.

        Integer i = 10;

        Character c = 'H';

    }
}
