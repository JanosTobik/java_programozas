/*
============================================================================
Name        : PrimitivTipusok.java
Author      : Tobik János
Version     : 1.0
Title		: Változók
Description	: Változók deklarálása és iniciálizálása.
			  Főbb változó típusok bemutatása.
============================================================================
*/

package hu.tobikjanos.java.bevezetes_1;

public class PrimitivTipusok {

    public static void main(String[] args)
    {
        /******************************
         * deklarálás és inicializálás *
         *******************************/

        //////////////////////////
        // deklarálás (létrehozás)
        //////////////////////////
        int x;


        ////////////////////////////////////////////////////////////////
        // deklarálás és inicializálás (létrehozás és kezdeti értékadás)
        ////////////////////////////////////////////////////////////////
        int y = 3;

        int apple = 14;

        int p, q = 40, r = 7;

        /*************************
         * főbb primitív típusok *
         *************************/

        char c;
        short s;
        int i;
        float f;
        double d;
        boolean b;
        byte by;
    }

}
