/*
============================================================================
Name        : KomplexTipusok.java
Author      : Tobik János
Version     : 1.0
Title		: Változók
Description	:
============================================================================
*/

package hu.tobikjanos.java.bevezetes_1;

public class KomplexTipusok {

    public static void main(String[] args)
    {
        /////////
        // tömbök
        /////////
        int tomb[] = new int[10];
        double[] tomb2 = new double[5];

        int tomb3[] = { 2, 4, 6, 3 };


        // tömb bizonyos elemének kinyerése

        int elsoElem = tomb3[0];        // == 2
        int masodikElem = tomb3[1];     // == 4

        int idx = 2;
        int indexedikElem = tomb3[idx]; // == 6

        int hibasIndexeles = tomb3[4];  // tömb indexelése 0-tól kezdődik, így a 4 elemű tömb utolsó elemét a 3. indexxel lehet kinyerni
                                        // 4. index kinyerésével hibára fut a program (Array index is out of bounds)


        ////////////////////////
        // több dimenziós tömbök
        ////////////////////////
        int[][] a = new int[10][20];
        double b[][] = new double[3][3];


        ////////////////////////////
        // stringek (karaktertömbök)
        ////////////////////////////
        String s = "Hello World";

        s = "123456";
    }

}
