/*
============================================================================
Name        : FileUtil.java
Author      : Tobik János
Version     : 1.0
Title		: File
Description	: Fájlok kezelése.
============================================================================
*/

package hu.tobikjanos.java.io_4.file;

import java.io.File;
import java.io.IOException;

public class FileUtil
{
    public static void main(String[] args)
    {
        String filePath = "src" + File.separator + "main" + File.separator + "resources" + File.separator + "input.txt";
        String dirPath = "src" + File.separator + "main" + File.separator + "resources";

        File file = new File(filePath);
        File dir= new File(dirPath);

        System.out.println("name: " + file.getName());
        System.out.println("exists: " + file.exists());
        System.out.println("is file: " + file.isFile());
        System.out.println("is directory: " + file.isDirectory());

        System.out.println("-------------------");

        System.out.println("name: " + dir.getName());
        System.out.println("exists: " + dir.exists());
        System.out.println("is file: " + dir.isFile());
        System.out.println("is directory: " + dir.isDirectory());

        System.out.println("-------------------");


        File newFile = new File(dirPath + File.separator + "newfile.txt");

        if (!newFile.exists())
        {
            try
            {
                newFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            System.out.println("name: " + newFile.getName());
            System.out.println("exists: " + newFile.exists());
            System.out.println("is file: " + newFile.isFile());
            System.out.println("is directory: " + newFile.isDirectory());
        }

        System.out.println("-------------------");

        if (newFile.exists())
        {
            newFile.delete();
            System.out.println("name: " + newFile.getName());
            System.out.println("exists: " + newFile.exists());
        }
    }
}
