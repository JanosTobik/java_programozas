/*
============================================================================
Name        : CharStreamReader.java
Author      : Tobik János
Version     : 1.0
Title		: CharStream
Description	: Karakter(ek) olvasása.
============================================================================
*/

package hu.tobikjanos.java.io_4.charstream;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CharStreamReader
{
    public static void main(String[] args)
    {
        /**
         * BufferedReader
         * - bufferelt tartalom, képes egész sort beolvasni
         *
         * CharArrayReader / StringReader
         * - char array-ből vagy string-ből olvas
         *
         * FileReader
         * - fájl olvasására szolgál
         */


        FileReader fileReader = null;
        try
        {
            fileReader = new FileReader("src/main/resources/input.txt");
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }


        /**
         * BufferedReader
         */
        BufferedReader bufferedReader1 = new BufferedReader(fileReader);
        try
        {
            int c;
            while ((c = bufferedReader1.read()) != -1)
            {
                System.out.println("c = " + (char)c);
            }
            bufferedReader1.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


        /**
         * NOTE: FileReader a fájl végére ért, ezért nem tud semmit már beolvasni.
         * Kommentezni kell a nem használatos részt, vagy új FileReader objektumot kell inicializálni.
         */
//        BufferedReader bufferedReader2 = new BufferedReader(fileReader);
//        String line;
//        try
//        {
//            while ((line = bufferedReader2.readLine()) != null)
//            {
//                System.out.println("line = " + line);
//            }
//            bufferedReader2.close();
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }

        /**
         * Fontos: a fileReader objektumot le kell zárni ha végzett a fájl beolvasásával.
         */
        try
        {
            fileReader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }
}
