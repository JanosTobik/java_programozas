/*
============================================================================
Name        : CharStreamWriter.java
Author      : Tobik János
Version     : 1.0
Title		: CharStream
Description	: Karakter(ek) írása.
============================================================================
*/

package hu.tobikjanos.java.io_4.charstream;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CharStreamWriter
{
    public static void main(String... args)
    {
        /**
         * BufferedWriter
         * - bufferelt tartalom
         *
         * CharArrayWriter / StringWriter
         * - char array-be vagy string-be ír
         *
         * FileWriter
         * - fájl írására szolgál
         *
         * PrintWriter
         * - formatált adat írása: print, println, printf
         */


        FileWriter fileWriter = null;
        try
        {
            fileWriter = new FileWriter("src/main/resources/output.txt");
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        /**
         * BufferedWriter
         */
//        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
//        try
//        {
//            bufferedWriter.write("This is the output file!");
//            bufferedWriter.newLine();
//            bufferedWriter.append("Second line");
//            bufferedWriter.newLine();
//            bufferedWriter.append("Third line");
//            bufferedWriter.close();
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }

        /**
         * PrintWriter
         */
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("A line by PrintWriter.");
        printWriter.printf("%d * %d = %d", 2, 3, 2*3);
        printWriter.close();


        /**
         * Fontos: a fileWriter objektumot le kell zárni ha végzett a fájl írásával.
         */
        try
        {
            fileWriter.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
