/*
============================================================================
Name        : ByteStreamWriter.java
Author      : Tobik János
Version     : 1.0
Title		: ByteStream
Description	: Byte írása.
============================================================================
*/

package hu.tobikjanos.java.io_4.bytestream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ByteStreamWriter
{
    public static void main(String[] args)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(101);
        byteArrayOutputStream.write(102);
        byteArrayOutputStream.write(32);

        try
        {
            byteArrayOutputStream.write(new byte[]{ 88, 89, 90 });
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        System.out.println(byteArrayOutputStream.toString());


        try
        {
            byteArrayOutputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
