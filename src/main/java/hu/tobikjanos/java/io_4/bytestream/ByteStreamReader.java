/*
============================================================================
Name        : ByteStreamReader.java
Author      : Tobik János
Version     : 1.0
Title		: ByteStream
Description	: Byte beolvasása.
============================================================================
*/

package hu.tobikjanos.java.io_4.bytestream;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ByteStreamReader
{
    public static void main(String[] args)
    {
        /**
         * ByteArrayInputStream
         * - byte olvasás byte array-ből
         *
         * FileInputStream
         * - byte olvasás fájlból
         */

        try
        {
            FileInputStream fileInputStream = new FileInputStream("src/main/resources/input.txt");
            int c;
            while ((c = fileInputStream.read()) != -1)
            {
                System.out.println("c = " + (char)c);
            }
            fileInputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }



        byte[] byteArray = { 0, 1, 2, 3, 4, -1, 6 };
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
        int b;
        while ((b = byteArrayInputStream.read()) != -1)
        {
            System.out.println("b = " + b);
        }

        try
        {
            byteArrayInputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
