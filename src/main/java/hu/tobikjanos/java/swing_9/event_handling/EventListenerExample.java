/*
============================================================================
Name        : EventListenerExample.java
Author      : Tobik János
Version     : 1.0
Title		: Event Listener
Description	: Event Listener használata Swingben
              Az EventListener interfész definiálja azon metódusokat, amelyeket implementálni kell.
              Nincs alapértelmezett működés.
============================================================================
*/

package hu.tobikjanos.java.swing_9.event_handling;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class EventListenerExample
{
    private JFrame mainFrame;
    private JPanel controlPanel;
    private JLabel headerLabel;
    private JLabel statusLabel;

    private Integer buttonClick = 0;

    public static void main(String[] args)
    {
        EventListenerExample eventListenerExample = new EventListenerExample();
        eventListenerExample.setupActionListener();
        eventListenerExample.setupComponentListener();
    }

    public EventListenerExample()
    {
        setupGUI();
    }

    private void setupGUI()
    {
        mainFrame = new JFrame("Java Swing Application - EventListener Example");
        mainFrame.setSize(400, 400);
        mainFrame.setLayout(new GridLayout(3, 1));
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setVisible(true);

        headerLabel = new JLabel("", JLabel.CENTER);
        statusLabel = new JLabel("", JLabel.CENTER);

        controlPanel = new JPanel();
        controlPanel.setLayout(new FlowLayout());

        mainFrame.add(headerLabel);
        mainFrame.add(controlPanel);
        mainFrame.add(statusLabel);
    }

    private void setupActionListener()
    {
        headerLabel.setText("Event Listener: Action Listener");

        JPanel panel = new JPanel();
        panel.setSize(100, 100);

        JLabel label = new JLabel("Érték: " + buttonClick);
        JButton button = new JButton("Increment");
        button.addActionListener(actionEvent ->
        {
            buttonClick++;
            label.setText("Érték: " + buttonClick);
        });

        panel.add(button);
        panel.add(label);

        controlPanel.add(panel);
    }

    private void setupComponentListener()
    {
        headerLabel.setText("Event Listener: Component Listener");

        JPanel panel = new JPanel();
        panel.setSize(100, 100);

        JLabel label = new JLabel("Hello World!", JLabel.CENTER);
        label.addComponentListener(new ComponentListener()
        {
            @Override
            public void componentResized(ComponentEvent e)
            {
                statusLabel.setText(statusLabel.getText() + e.getComponent().getClass().getSimpleName() + " resized. ");
            }

            @Override
            public void componentMoved(ComponentEvent e)
            {
                statusLabel.setText(statusLabel.getText() + e.getComponent().getClass().getSimpleName() + " moved. ");
            }

            @Override
            public void componentShown(ComponentEvent e)
            {
                statusLabel.setText(statusLabel.getText() + e.getComponent().getClass().getSimpleName() + " shown. ");
            }

            @Override
            public void componentHidden(ComponentEvent e)
            {
                statusLabel.setText(statusLabel.getText() + e.getComponent().getClass().getSimpleName() + " hidden. ");
            }
        });

        label.setVisible(false);
        label.setVisible(true);

        panel.add(label);

        controlPanel.add(panel);
    }
}
