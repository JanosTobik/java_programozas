/*
============================================================================
Name        : BorderLayoutExample.java
Author      : Tobik János
Version     : 1.0
Title		: Border Layout
Description	: Border Layout használata Swingben
============================================================================
*/

package hu.tobikjanos.java.swing_9.layout;

import javax.swing.*;
import java.awt.*;

public class BorderLayoutExample
{
    private JFrame mainFrame;
    private JPanel controlPanel;
    private JLabel headerLabel;
    private JLabel statusLabel;

    public static void main(String[] args)
    {
        BorderLayoutExample borderLayoutExample = new BorderLayoutExample();
        borderLayoutExample.setupBorderLayout();
    }

    public BorderLayoutExample()
    {
        setupGUI();
    }

    private void setupGUI()
    {
        mainFrame = new JFrame("Java Swing Application - BorderLayout Example");
        mainFrame.setSize(400, 400);
        mainFrame.setLayout(new GridLayout(3, 1));
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setVisible(true);

        headerLabel = new JLabel("", SwingConstants.CENTER);
        statusLabel = new JLabel("", SwingConstants.CENTER);

        controlPanel = new JPanel();
        controlPanel .setLayout(new FlowLayout());

        mainFrame.add(headerLabel);
        mainFrame.add(controlPanel);
        mainFrame.add(statusLabel);
    }

    private void setupBorderLayout()
    {
        headerLabel.setText("Border layout");

        JPanel panel = new JPanel();
        panel.setBackground(Color.DARK_GRAY);
        panel.setSize(300, 300);

        BorderLayout layout = new BorderLayout();
        layout.setHgap(10);
        layout.setVgap(10);

        panel.setLayout(layout);
        panel.add(new JButton("Center"),BorderLayout.CENTER);
//        panel.add(new JButton("Line Start"),BorderLayout.LINE_START);
//        panel.add(new JButton("Line End"),BorderLayout.LINE_END);
        panel.add(new JButton("East"),BorderLayout.EAST);
        panel.add(new JButton("West"),BorderLayout.WEST);
        panel.add(new JButton("North"),BorderLayout.NORTH);
        panel.add(new JButton("South"),BorderLayout.SOUTH);

        controlPanel.add(panel);
    }
}
