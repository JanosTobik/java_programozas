/*
============================================================================
Name        : SynchronizedThread.java
Author      : Tobik János
Version     : 1.0
Title		: Synchronized
Description	: Szálkezelés - Synchronized objektumok használata
============================================================================
*/

package hu.tobikjanos.java.szalkezeles_7;

public class SynchronizedThread
{
    public static void main(String[] args)
    {
        MyClass myClass = new MyClass();
        Thread1 thread1 = new Thread1(myClass);
        Thread2 thread2 = new Thread2(myClass);
        thread1.run();
        thread2.run();
    }

    private static class Thread1 implements Runnable
    {
        private MyClass myClass;

        public Thread1(MyClass myClass)
        {
            this.myClass = myClass;
        }

        @Override
        public void run()
        {
            myClass.printMyClass(20);
        }
    }

    private static class Thread2 implements Runnable
    {
        private MyClass myClass;

        public Thread2(MyClass myClass)
        {
            this.myClass = myClass;
        }

        @Override
        public void run()
        {
            myClass.printMyClass(2);
        }
    }

    private static class MyClass
    {
        public synchronized void printMyClass(int number)
        {
            for (int i = 0; i < 5; i++)
            {
                System.out.println("number * i = " + number * i);
                try
                {
                    Thread.sleep(500);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
