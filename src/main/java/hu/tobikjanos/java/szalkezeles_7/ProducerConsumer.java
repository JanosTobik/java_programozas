/*
============================================================================
Name        : ProducerConsumer.java
Author      : Tobik János
Version     : 1.0
Title		: Producer és Consumer
Description	: Szálkezelés - synchronized metódusok, signal-ozás szálak között
============================================================================
*/

package hu.tobikjanos.java.szalkezeles_7;

public class ProducerConsumer
{
    public static void main(String[] args)
    {
        Buffer buffer = new Buffer();
        Producer producer = new Producer(buffer, 1);
        Consumer consumer = new Consumer(buffer, 2);

        producer.start();
        consumer.start();
    }

    /**
     * Első szál, amely feltölti a buffer értéket
     */
    public static class Producer extends Thread
    {
        private Buffer buffer;
        private int number;

        public Producer(Buffer buffer, int number)
        {
            this.buffer = buffer;
            this.number = number;
        }

        @Override
        public void run()
        {
            // az egyes iterációkban buffer értékének megváltoztatása
            for (int i = 0; i < 10; i++)
            {
                buffer.put(i);
                System.out.println("number = " + number);
                try
                {
                    Thread.sleep(6000);
                }
                catch (InterruptedException e)
                {}
            }
        }
    }


    /**
     * Második szál, amely kiveszi a buffer tartalmát
     */
    public static class Consumer extends Thread
    {
        private Buffer buffer;
        private int number;

        public Consumer(Buffer buffer, int number)
        {
            this.buffer = buffer;
            this.number = number;
        }

        @Override
        public void run()
        {
            int value;
            // az egyes iterációkban buffer értékének kinyerése
            for (int i = 0; i < 10; i++)
            {
                value = buffer.get();
                System.out.println("number = " + number);
                System.out.println("value = " + value);
            }
        }
    }

    /**
     * Buffer objektum
     */
    public static class Buffer
    {
        private int content;
        private boolean available = false;

        // synchronized metódus, ami lock-olja az objektumot - Buffer értékének kinyerése
        public synchronized int get()
        {
            while (available == false)
            {
                try
                {
                    wait();         // feloldja a synchronized miatti lock-ot, amíg nem kap a másik száltól signal-t
                }
                catch (InterruptedException e)
                {}
            }
            available = false;      // azért kell, hogy a következő iterációban a 'wait()' metódus lefusson
            notifyAll();            // signal küldése a másik szálnak, hogy ne várakozzon tovább, futását folytathatja
            return content;
        }

        // synchronized metódus, ami lock-olja az objektumot - Buffer értékének megváltoztatása
        public synchronized void put(int value)
        {
            while (available == true)
            {
                try
                {
                    wait();         // feloldja a synchronized miatti lock-ot, amíg nem kap a másik száltól signal-t
                }
                catch (InterruptedException e)
                {}
            }
            content = value;
            available = true;       // azért kell, hogy a következő iterációban a 'wait()' metódus lefusson
            notifyAll();            // signal küldése a másik szálnak, hogy ne várakozzon tovább, futását folytathatja
        }
    }
}
