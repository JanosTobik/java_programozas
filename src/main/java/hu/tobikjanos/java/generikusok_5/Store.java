/*
============================================================================
Name        : Store.java
Author      : Tobik János
Version     : 1.0
Title		: Generikusok
Description	: Saját generikus osztály.
============================================================================
*/

package hu.tobikjanos.java.generikusok_5;

public class Store<T>
{
    private T[] objects;
    private int size;

    public Store(int size)
    {
        this.size = size;
        this.objects = (T[])new Object[size];
    }

    public void put(T object)
    {
        int idx = 0;
        while (idx < this.size && this.objects[idx] != null)
        {
            idx++;
        }
        if (idx == this.size)   // hibakezelés
        {
            return;
        }
        this.objects[idx] = object;
    }

    public T get(int idx)
    {
        return objects[idx];
    }

    public int size()
    {
        return this.size;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.objects.length; i++)
        {
            sb.append(i + ". = ").append(objects[i]).append("\n");
        }
        return sb.toString();
    }
}
