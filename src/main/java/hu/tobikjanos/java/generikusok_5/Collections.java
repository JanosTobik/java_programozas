/*
============================================================================
Name        : Collections.java
Author      : Tobik János
Version     : 1.0
Title		: Generikusok
Description	: Generikus collection használata.
============================================================================
*/

package hu.tobikjanos.java.generikusok_5;

import java.util.*;

public class Collections
{
    public static void main(String[] args)
    {
        /**
         * Egyszerű lista - List interface
         */
        List<String> stringList = new ArrayList<>();
        stringList.add("Hello");
        stringList.add("World");
        stringList.add("!");

        System.out.println(stringList.toString());
        System.out.println(stringList.size());
        System.out.println(stringList.isEmpty());
        System.out.println("\n--------------------------------\n");

        /**
         * Kulcs-érték - Map interface
         */
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Hello");
        map.put(0, "World");
        map.put(20, "!");
        System.out.println(map.get(0));
        System.out.println(map.get(20));
        System.out.println(map.get(1));
        System.out.println("\n--------------------------------\n");

        /**
         * További interface-k:
         * - Set: minden objektumot egyszer tárol, rendezetlen
         * - Queue: First In First Out
         * - Stack: Last In First Out
         */


        /**
         * Iterator
         */
        Iterator<String> sIter = stringList.iterator();
        while (sIter.hasNext())
        {
            System.out.println("sIter.next() = " + sIter.next());
        }
        System.out.println("\n--------------------------------\n");

        Iterator<Integer> iIter = map.keySet().iterator();
        while (iIter.hasNext())
        {
            if (iIter.next() == 0)
            {
                iIter.remove();
            }
        }
        System.out.println(map.get(0));
    }
}
