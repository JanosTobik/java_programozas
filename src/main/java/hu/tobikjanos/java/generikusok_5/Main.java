/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: Generikusok
Description	:
============================================================================
*/

package hu.tobikjanos.java.generikusok_5;

public class Main
{
    public static void main(String[] args)
    {
        Store<String> stringStore = new Store<>(7);
        stringStore.put("Hello");
        stringStore.put("World");
        stringStore.put("!");
        stringStore.put("negyedik");
        stringStore.put("otodik");

        System.out.println(stringStore.toString());
        System.out.println("size: " + stringStore.size());
        System.out.println(stringStore.get(3));
        System.out.println("\n--------------------------------\n");


        /**
         * Primitív típusokat nem lehet megadni.
         */
        Store<Integer> integerStore = new Store<>(5);
        integerStore.put(100);
        integerStore.put(-17);
        integerStore.put(8);
        integerStore.put(21);

        System.out.println(integerStore.toString());
        System.out.println("size: " + integerStore.size());
        System.out.println(integerStore.get(1));
        System.out.println("\n--------------------------------\n");



        Store<Double> doubleStore = new Store<>(4);
        doubleStore.put(1.2);
        doubleStore.put(-321.788);
        doubleStore.put(0.01);
        doubleStore.put(678.901);

        System.out.println(doubleStore.toString());
        System.out.println("size: " + doubleStore.size());
        System.out.println(doubleStore.get(2));
    }
}
