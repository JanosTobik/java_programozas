/*
============================================================================
Name        : RegularisKifejezes.java
Author      : Tobik János
Version     : 1.0
Title		: Reguláris kifejezések
Description	: https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
============================================================================
*/

package hu.tobikjanos.java.utils_6.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularisKifejezes
{
    public static void main(String[] args)
    {
        /**
         * String.matches()
         */
        String minta = "x";

        System.out.println("x".matches(minta));
        System.out.println("--------------------------------------------------------------");

        minta = "x.";
        System.out.println("xa".matches(minta));
        System.out.println("xE".matches(minta));
        System.out.println("xaa".matches(minta));
        System.out.println("--------------------------------------------------------------");

        minta = "a*b*";
        System.out.println("aaaaaabb".matches(minta));
        System.out.println("aabbbbbbbbb".matches(minta));
        System.out.println("aaaaabbbccc".matches(minta));
        System.out.println("--------------------------------------------------------------");

        minta = "[a-zA-Z ]*";
        System.out.println("Hello World".matches(minta));
        System.out.println("hello VILAG".matches(minta));
        System.out.println("hello!!!".matches(minta));
        System.out.println("--------------------------------------------------------------");

        minta = "[a-zA-Z 0-9]*";
        System.out.println("Hello 101234 World".matches(minta));
        System.out.println("hello VILAG 8765".matches(minta));
        System.out.println("hello dfgeh $ 543".matches(minta));
        System.out.println("--------------------------------------------------------------");

        minta = "dog|cat|cow";
        System.out.println("cat".matches(minta));
        System.out.println("dog".matches(minta));
        System.out.println("bird".matches(minta));
        System.out.println("--------------------------------------------------------------");

        minta = "[a-zA-z]{4}";
        System.out.println("abcd".matches(minta));
        System.out.println("abcde".matches(minta));
        System.out.println("aaaa".matches(minta));
        System.out.println("--------------------------------------------------------------");

        minta = "\\d{3,6}";
        System.out.println("123".matches(minta));
        System.out.println("123456".matches(minta));
        System.out.println("1234567".matches(minta));
        System.out.println("--------------------------------------------------------------");


        /**
         * Pattern
         */

        String line = "Object ID is GT4521!";
        Pattern pattern = Pattern.compile("[A-Za-z]*\\d+");
        Matcher matcher = pattern.matcher(line);

        if (matcher.find())
        {
            System.out.println(line.substring(matcher.start(), matcher.end()));
            System.out.println(matcher.group(0));
        }
        System.out.println("--------------------------------------------------------------");

        line = "hello world DOG 47457 dog $$ŁŁ cat CAT";
        pattern = Pattern.compile("(.*)\\s(\\d+)\\s(.*)");
        matcher = pattern.matcher(line);

        if (matcher.find())
        {
            System.out.println("group 0: " + matcher.group(0));
            System.out.println("group 1: " + matcher.group(1));
            System.out.println("group 2: " + matcher.group(2));
            System.out.println("group 3: " + matcher.group(3));
        }
        System.out.println("--------------------------------------------------------------");

        line = "dog dog doggy dog dog dog";
        pattern = Pattern.compile("\\b(dog)\\b");
        matcher = pattern.matcher(line);
        int cnt = 0;

        while (matcher.find())
        {
            cnt++;
            System.out.println("cnt = " + cnt);
            System.out.println(line.substring(matcher.start(), matcher.end()));
        }
        System.out.println("--------------------------------------------------------------");


        line = "My cat barks. All cats say woof.";
        pattern = Pattern.compile("cat");
        matcher = pattern.matcher(line);

        String result = matcher.replaceAll("dog");
        System.out.println("result = " + result);
        System.out.println("--------------------------------------------------------------");


        line = "fooaaabbbbfooabbfooaaabb";
        pattern = Pattern.compile("a+b+");
        matcher = pattern.matcher(line);
        StringBuffer stringBuffer = new StringBuffer();

        while (matcher.find())
        {
            matcher.appendReplacement(stringBuffer, " ### ");
        }
        System.out.println("replacement: " + stringBuffer.toString());

    }
}
