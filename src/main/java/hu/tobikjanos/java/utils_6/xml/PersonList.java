/*
============================================================================
Name        : PersonList.java
Author      : Tobik János
Version     : 1.0
Title		: XML
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "personList")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonList
{
    @XmlElement(name = "person")
    private List<Person> personList = new ArrayList<>();

    public List<Person> getPerson()
    {
        return personList;
    }
}
