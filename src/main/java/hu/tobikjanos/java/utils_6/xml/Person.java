/*
============================================================================
Name        : Person.java
Author      : Tobik János
Version     : 1.0
Title		: XML
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.xml;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "person")
@XmlAccessorType(XmlAccessType.FIELD)
public class Person
{
    @XmlElement
    private String name;
    @XmlElement
    private Integer age;
    @XmlElement
    private String job;

    public Person()
    {
    }

    public Person(String name, Integer age, String job)
    {
        this.name = name;
        this.age = age;
        this.job = job;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getAge()
    {
        return age;
    }

    public void setAge(Integer age)
    {
        this.age = age;
    }

    public String getJob()
    {
        return job;
    }

    public void setJob(String job)
    {
        this.job = job;
    }

    @Override
    public String toString()
    {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", job='" + job + '\'' +
                '}';
    }
}
