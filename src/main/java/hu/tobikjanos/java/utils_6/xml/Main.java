/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: XML
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class Main
{
    public static void main(String[] args) throws JAXBException
    {
        /**
         * Teszt adatok létrehozása
         */
        Person person1 = new Person("name1", 10, "job1");
        Person person2 = new Person("name2", 20, "job2");
        Person person3 = new Person("name3", 30, "job3");
        Person person4 = new Person("name4", 40, "job4");

        PersonList personList = new PersonList();
        personList.getPerson().add(person1);
        personList.getPerson().add(person2);
        personList.getPerson().add(person3);
        personList.getPerson().add(person4);

        File file = new File("src/main/resources/person.xml");

        /**
         * JAXB context példányosítása
         */
        JAXBContext jaxbContext = JAXBContext.newInstance(PersonList.class, Person.class);


        /**
         * Marshalling - java objektumból xml
         */
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(personList, file);


        /**
         * Unmarshalling - xml-ből java objektum
         */
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        PersonList result = (PersonList) unmarshaller.unmarshal(file);

        for (Person p : result.getPerson())
        {
            System.out.println(p);
        }
    }
}
