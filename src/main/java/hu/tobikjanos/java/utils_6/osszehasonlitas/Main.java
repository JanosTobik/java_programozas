/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: Összehasonlítás
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.osszehasonlitas;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        /**
         * Comparable
         */
        MyComparableClass myComparableClass1 = new MyComparableClass(200, "ASD", 13.8);
        MyComparableClass myComparableClass2 = new MyComparableClass(521, "ASD", 13.8);
        MyComparableClass myComparableClass3 = new MyComparableClass(185, "ASD", 13.8);

        System.out.println(myComparableClass1.compareTo(myComparableClass2));

        List<MyComparableClass> list1 = Arrays.asList(myComparableClass1, myComparableClass2, myComparableClass3);
        Collections.sort(list1);    // rendezés
        for (MyComparableClass element: list1)
        {
            System.out.println("element = " + element);
        }
        System.out.println("\n--------------------------------\n");



        /**
         * Comparator
         */
        MyClass myClass1 = new MyClass(521, "ASD", 35.8);
        MyClass myClass2 = new MyClass(185, "ASD", 79.2);
        MyClass myClass3 = new MyClass(200, "ASD", 13.11);
        List<MyClass> list2 = Arrays.asList(myClass1, myClass2, myClass3);

        Collections.sort(list2, new MyComparator());

        for (MyClass element : list2)
        {
            System.out.println("element = " + element);
        }

    }
}
