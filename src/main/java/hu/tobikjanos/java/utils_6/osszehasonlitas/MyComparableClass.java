/*
============================================================================
Name        : MyComparableClass.java
Author      : Tobik János
Version     : 1.0
Title		: Összehasonlítás
Description	: Saját comparable osztály létrehozása.
============================================================================
*/

package hu.tobikjanos.java.utils_6.osszehasonlitas;

public class MyComparableClass implements Comparable<MyComparableClass>
{
    private Integer a;
    private String b;
    private Double c;

    public MyComparableClass()
    {
    }

    public MyComparableClass(Integer a, String b, Double c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public int compareTo(MyComparableClass o)
    {
        return this.a - o.getA();
    }

    public Integer getA()
    {
        return a;
    }

    public void setA(Integer a)
    {
        this.a = a;
    }

    public String getB()
    {
        return b;
    }

    public void setB(String b)
    {
        this.b = b;
    }

    public Double getC()
    {
        return c;
    }

    public void setC(Double c)
    {
        this.c = c;
    }

    @Override
    public String toString()
    {
        return a + " + " + b + " + " + c;
    }

}
