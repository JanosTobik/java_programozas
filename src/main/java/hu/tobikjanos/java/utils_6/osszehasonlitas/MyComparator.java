/*
============================================================================
Name        : MyComparator.java
Author      : Tobik János
Version     : 1.0
Title		: Összehasonlítás
Description	: Saját comparator osztály létrehozása.
============================================================================
*/

package hu.tobikjanos.java.utils_6.osszehasonlitas;

import java.util.Comparator;

public class MyComparator implements Comparator<MyClass>
{
    @Override
    public int compare(MyClass o1, MyClass o2)
    {
        return (int) (o1.getC() - o2.getC());
    }
}
