/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: Enumok
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.enumok;

public class Main
{
    public static void main(String[] args)
    {
        /**
         * Enumok
         * Előre meghatározott, összetartozó elemek halmaza
         */
        DaysEnum daysEnum = DaysEnum.SUNDAY;
        switch (daysEnum)
        {
            case MONDAY:
                System.out.println("Hetfo");
                break;
            case TUESDAY:
                System.out.println("Kedd");
                break;
            case WEDNESDAY:
                System.out.println("Szerda");
                break;
            case THURSDAY:
                System.out.println("Csutortok");
                break;
            case FRIDAY:
                System.out.println("Pentek");
                break;
            case SATURDAY:
                System.out.println("Szombat");
                break;
            case SUNDAY:
                System.out.println("Vasarnap");
                break;
        }

        System.out.println("\n--------------------------------\n");



        Days2Enum days2Enum = Days2Enum.TUESDAY;
        System.out.println("ENG= " + days2Enum.getEng());
        System.out.println("HUN= " + days2Enum.getHun());

    }
}
