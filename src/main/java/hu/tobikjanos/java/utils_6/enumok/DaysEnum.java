/*
============================================================================
Name        : DaysEnum.java
Author      : Tobik János
Version     : 1.0
Title		: Enumok
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.enumok;

public enum DaysEnum
{
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
}
