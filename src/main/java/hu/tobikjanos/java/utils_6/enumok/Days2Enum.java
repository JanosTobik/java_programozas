/*
============================================================================
Name        : Days2Enum.java
Author      : Tobik János
Version     : 1.0
Title		: Enumok
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.enumok;

public enum Days2Enum
{
    MONDAY("MONDAY", "HETFO"),
    TUESDAY("TUESDAY", "KEDD"),
    WEDNESDAY("WEDNESDAY", "SZERDA"),
    THURSDAY("THURSDAY", "CSUTORTOK"),
    FRIDAY("FRIDAY", "PENTEK"),
    SATURDAY("SATURDAY", "SZOMBAT"),
    SUNDAY("SUNDAY", "VASARNAP");

    private String eng;
    private String hun;

    Days2Enum(String eng, String hun)
    {
        this.eng = eng;
        this.hun = hun;
    }

    public String getEng()
    {
        return eng;
    }

    public String getHun()
    {
        return hun;
    }
}
