/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: Szerializáció
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.szerializacio;

import java.io.*;

public class Main
{
    public static void main(String[] args)
    {
        /**
         * Serialization: byte stream konvertálása objektumból, byte stream tárolása, továbbküldése, stb.
         * Deserialization: byte stream konvertálása objektummá
         */
        String path = "src/main/resources/myclassfile.txt";
        MyClass mc = new MyClass(100, "Hello World!", 12.3);
        try
        {
            /**
             * Írás (Szerializáció)
             */
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(mc);
            objectOutputStream.close();

            /**
             * Olvasás (Deszerializáció)
             * transient változó értéke nem mentődik el, ezért annak értéke a default érték lesz (jelen esetben null)
             */
            FileInputStream fileInputStream = new FileInputStream(path);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            MyClass object = (MyClass) objectInputStream.readObject();
            System.out.println("deserialization= " + object.toString());
        }
        catch (IOException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}
