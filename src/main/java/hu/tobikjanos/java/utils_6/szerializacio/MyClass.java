/*
============================================================================
Name        : MyClass.java
Author      : Tobik János
Version     : 1.0
Title		: Szerializáció
Description	: Szerializálandó osztály. A 'transient' kulcsszóval tudjuk definiálni azon változókat,
              amelyeket nem szeretnénk szerializálni. Ezek deszerializáció során a default értéket kapják.
              Ha nincs default érték, akkor null lesz.
============================================================================
*/

package hu.tobikjanos.java.utils_6.szerializacio;

import java.io.Serializable;

public class MyClass implements Serializable
{
    private Integer a;
    private String b;
    private transient Double c;

    public MyClass()
    {
    }

    public MyClass(Integer a, String b, Double c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Integer getA()
    {
        return a;
    }

    public void setA(Integer a)
    {
        this.a = a;
    }

    public String getB()
    {
        return b;
    }

    public void setB(String b)
    {
        this.b = b;
    }

    public Double getC()
    {
        return c;
    }

    public void setC(Double c)
    {
        this.c = c;
    }

    @Override
    public String toString()
    {
        return a + " + " + b + " + " + c;
    }
}
