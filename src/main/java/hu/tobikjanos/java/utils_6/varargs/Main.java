/*
============================================================================
Name        : Main.java
Author      : Tobik János
Version     : 1.0
Title		: Varargs
Description	:
============================================================================
*/

package hu.tobikjanos.java.utils_6.varargs;

public class Main
{
    public static void main(String[] args)
    {
        foo("Hello", new String[]{"a", "b", "c", "D", "E", "F", "x"});

        System.out.println("\n--------------------------------\n");

        bar("Hello", "a", "b", "c", "D", "E", "F", "x");
    }

    private static void foo(String arg1, String[] args)
    {
        System.out.println("arg1 = " + arg1);
        for (String arg : args)
        {
            System.out.println("arg = " + arg);
        }
    }

    private static void bar(String arg1, String... args)
    {
        System.out.println("arg1 = " + arg1);
        for (String arg : args)
        {
            System.out.println("arg = " + arg);
        }
    }
}
