# Java programozás

## Témakörök

1. Bevezetés
2. Vezérlési szerkezetek
3. OOP (Object Orient Programming)
4. IO (Input/Output)
5. Generikusok
6. Hasznos dolgok
7. Szálkezelés
8. Java 8